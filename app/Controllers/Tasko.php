<?php namespace App\Controllers;
use App\Models\TaskoModel;
use App\Models\TaskerofaritaModel;
use App\Models\ParamModel;

class Tasko extends BaseController
{
    public function __construct()
    {

        $session = \Config\Services::session();
        $userapp = $session->userapp;
        if($userapp == false || $userapp  = NULL || empty($userapp ))
        {
            echo view('login.php'); 
            exit;
        }
    }
    
    public function index()
	{
        $this->liste();
	}

    public function liste($type = '5' )
	{
        $session = \Config\Services::session();
        $data['view']['title'] = "Taches";
        $data['typeapp'] = "tasko";
        $data['dbtable'] = "tasko";
    
        $p['userapp'] = $session->userapp;
        $p['useradmin'] = $session->useradmin;
   
        $p['tstatus'] = '5';
        if ($type == '3') $p['tstatus'] = '3';
        if ($type == '7') $p['tstatus'] = '7';
        if ($type == '0') $p['tstatus'] = '0';
        $model = new TaskoModel();
        $data['t'] = $model->getTaskoj($p);

        $page = new Page();
        $page->showme('tasko-liste',$data);

    }
    
    function ajout(){
        $data['dbtable'] = "tasko";
        $data['view']['title'] = "Ajout d'une tache";
        $data['action'] = 'add';
        $data['r']['tnom'] = "";
        $data['r']['tdesc'] = "";
        $data['r']['tdurationreal'] = 0;
        $data['r']['tdurationestim'] = 0;
        $data['r']['ttermindate'] = "";
        $data['r']['tcateg'] = "";
        $param = new ParamModel();
        $data['categs'] = $param->getparams('categ');
        $page = new Page();
        $page->showme('tasko-edit',$data);
    }

    function add(){
       /* $session = \Config\Services::session();
         $wudate = date('Y-m-d');
         $d = [
            'tnom'  => $this->request->getVar('tnom'),
            'tdesc'  => $this->request->getVar('tdesc'),
            'tdurationestim'  => $this->request->getVar('tdurationestim'),
            'ttermindate'  => $this->request->getVar('ttermindate'),
            'tstatus'  => $this->request->getVar('tstatus'),
            'tuzanto'  => $session->userapp,        
            'tdatcrt' =>  $wudate ,
            'tusrcrt' => $session->userapp,
            'tdatupd' =>  $wudate ,
            'tusrupd' => $session->userapp
        ];*/
        $d = $this->addupd('a');
        $db = db_connect();
        $db->table('tasko')->insert($d);
        $this->liste();
	}

    function edit($id){
        $data['dbtable'] = "tasko";
        $data['view']['title'] = "Modification d'une tache";
        $data['action'] = 'upd';
        $strsql = "select * from tasko where tid = $id";
        $db = db_connect();
        $query = $db->query($strsql);
        $data['r'] = $query->getRowArray();
        $model = new TaskerofaritaModel();
        $data['nbtf'] = $model->getNbTaskerofaritaj($id);
        $param = new ParamModel();
        $data['categs'] = $param->getparams('categ');


        $page = new Page();
        $page->showme('tasko-edit',$data);
   
    }

	public function upd() {


        helper(['form', 'url']);
        $validation =  \Config\Services::validation();
        $validation->setRule('tnom', 'tnom', 'required');

        if (! $this->validate([
            'tnom' => "required"]))
        {
          //echo 'pas bon ';
           $errors = $validation->getErrors();
          // var_dump($errors);
           //$this->edit($id);
           $data['dbtable'] = "tasko";
           $data['view']['title'] = "Modification d'une tache";
           $data['errors'] = 'Saisir une tâche';
           $data['action'] = 'upd';
           $id = $this->request->getVar('tid');
           $strsql = "select * from tasko where tid = $id";
         //  echo $strsql;
           $db = db_connect();
           $query = $db->query($strsql);
           $data['r'] = $query->getRowArray();

   
            $page = new Page();
           $page->showme('tasko-edit',$data);
        }
        else
        {
        //    echo 'bon';
 

        $date = date('Y-m-d');
 
        $id = $this->request->getVar('tid');
        $session = \Config\Services::session();
        $d = [
            'tnom'  => $this->request->getVar('tnom'),
            'tdesc'  => $this->request->getVar('tdesc'),
            'tdurationestim'  => $this->request->getVar('tdurationestim'),
            'ttermindate'  => $this->request->getVar('ttermindate'),
           
            'tstatus'  => $this->request->getVar('tstatus'),
            'tdatupd' =>  $date ,
            'tusrupd' => $session->userapp
        ];
        $d = $this->addupd('u');
        $db = db_connect();
        $builder = $db->table('tasko');
        $builder->where('tid', $id);
        $builder->update($d);


        $this->liste(); 
        }
    }

    function addupd($t){
        $wudate = date('Y-m-d');
        $session = \Config\Services::session();

        $d['tnom'] = $this->request->getVar('tnom');
        $d['tdesc'] = $this->request->getVar('tdesc');
        $d['tdurationestim'] = $this->request->getVar('tdurationestim');
        $d['ttermindate'] = $this->request->getVar('ttermindate');
        $d['tstatus'] = $this->request->getVar('tstatus');
        $d['tcateg'] = $this->request->getVar('tcateg');
        $d['tdatupd'] = $wudate;
        $d['tusrupd'] = $session->userapp;
        if ($t=="a"){
            $d['tdatcrt'] = $wudate;
            $d['tusrcrt'] =  $session->userapp;
            }
        return $d;
        }
/* ===== suppression etape 1 ===== */
function sup($id){
    $data['dbtable'] = "tasko";

    $data['action'] = 'del';


    $model = new TaskerofaritaModel();
    $data['nbtf'] = $model->getNbTaskerofaritaj($id);

    $param = new ParamModel();
    $data['categs'] = $param->getparams('categ');

    if ( $data['nbtf'] == 0){ $data['view']['title'] = "Suppression d'une tache";}
    else {$data['view']['title'] = "suppression non possible";}
    $strsql = "select * from tasko where tid = $id";
    $db = db_connect();
    $query = $db->query($strsql);
    $data['r'] = $query->getRowArray();
    $page = new Page();
    $page->showme('tasko-edit',$data);
   
}

/* ===== suppression reel ===== */
 function del(){
    $id = $this->request->getVar('tid');
    $db = db_connect();
    $builder = $db->table('tasko');
    $builder->where('tid', $id);
    $builder->delete();
    $this->liste(); 
}

public function vue($id)
{
    $data['dbtable'] = "tasko";
    $data['view']['title'] = "Réalisation d'une tache";
    //$data['action'] = 'upd';
   // $strsql = "select * from tasko where tid = $id";
    
   // $db = db_connect();
   // $query = $db->query($strsql);
   // $data['r'] = $query->getRowArray();


    //$p['userapp'] = $session->userapp;
    //$p['useradmin'] = $session->useradmin;
    $model = new TaskoModel();
    $data['t'] = $model->get_1($id);

    $session = \Config\Services::session();
    $p['userapp'] = $session->userapp;
    $p['useradmin'] = $session->useradmin;
    $p['tid'] = $id;
    $model = new TaskerofaritaModel();
    $data['tf'] = $model->getTaskerofaritaj($p);

  //  echo 'vue'.$data['t']['tnom'];
  //  var_dump($data);
    $page = new Page();
    $page->showme('tasko-tf',$data);
}
public function addtf()
{
    $session = \Config\Services::session();
    $tfduration =1;
    if ($this->request->getVar('tfduration') > 0){$tfduration = $this->request->getVar('tfduration');}
    $id = $this->request->getPostGet('tftid');
    $wudate = date('Y-m-d');
    $d = [
        'tftid'  => $this->request->getPostGet('tftid'),
        'tfuzanto' => $session->userapp,
        'tfdate'  => $this->request->getVar('tfdate'),    
        'tfdesc'  => $this->request->getVar('tfdesc'),    
        'tfduration'  => $tfduration,      
        'tfdatcrt' =>  $wudate ,
        'tfusrcrt' => $session->userapp,
        'tfdatupd' =>  $wudate ,
        'tfusrupd' => $session->userapp
    ];
   // var_dump($d);
    
    $db = db_connect();
    $db->table('taskerofarita')->insert($d);
    // mise à jour de la table tasko

    // calul de la durée total réalisé et date de dernière tache réalisée
    $model = new TaskoModel();
    $model->updDurationreal($id);
    $model->updDateReal($id,$wudate);  

    $this->vue($id);

}
//------ Suppression et création de la table 

	public function crttab(){
        $db = db_connect();
        $strsql = "DROP TABLE `tasko`;";
        echo '<br/>'.$strsql;
        $db = db_connect();
       $db->query($strsql);
	$strsql = "CREATE TABLE `tasko` (
        `tid` INTEGER,
        `tprojecto` TEXT,
        `tcateg` TEXT,
        `tuzanto` TEXT,
        `tnom` TEXT,
        `tdesc` TEXT,
        `tdurationestim` INT,
        `tdurationreal` INT,
        `ttermindate` TEXT,
        `tpriorit` INT,
        `tstatus` TEXT,
        `tdatreal`	TEXT,
        `tdatcrt`	TEXT,
    	`tusrcrt`	TEXT,
    	`tdatupd`	TEXT,
    	`tusrupd`	TEXT,
         PRIMARY KEY(`tid`));";
        echo '<br/>'.$strsql;

    $db->query($strsql);
    }

    public function updtab1(){
    // ajout du champ tdatreal : date de dernière utilisation
        $db = db_connect();
        $strsql = "ALTER TABLE tasko  ADD tdatreal TEXT";
         echo '<br/>'.$strsql;
        $db = db_connect();
       $db->query($strsql);
    }


}

