<?php namespace App\Controllers;
use CodeIgniter\Controller;

    class Page extends Controller {

    public function index()
    {
        return view('welcome_message');
    }

    public function showme($page = 'home',$data)
    {
        if ( ! is_file(APPPATH.'/Views/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter
        $session = \Config\Services::session();
        //$session->useradmin = "A";
        $data['userapp'] = $session->userapp;
        $data['useradmin'] = $session->useradmin;

       echo view('template/header', $data);
       echo view($page.'.php', $data);
       echo view('template/footer', $data);
    }
}
