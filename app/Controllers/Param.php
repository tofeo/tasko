<?php namespace App\Controllers;
use \App\Controllers\BaseController;
use \App\Controllers\Page;

class Param extends BaseController { 
  public static  $table = 'param';
  public static  $page = 'param';
   
  public function __construct()
  {
      $session = \Config\Services::session();
      $userapp = $session->userapp;
      if($userapp == false || $userapp  = NULL || empty($userapp ))
      {
          $data['msgerr'] = "";
          $page = new Page();
          $page->admin('login',$data);
          exit;
      }
  }


// liste 
    public function index()
	{
   
       // echo "param";
        $this->liste();
	}

    public function liste()
	{
        $data['view']['title'] = "Paramètres";
        $data['page'] = self::$page;
        $strsql = "SELECT *  FROM ".self::$table." order by patype, ordre ";
       // echo $strsql;
        $db = db_connect();
        $query = $db->query($strsql);
        $data['t'] = $query->getResult();
        $page = new Page();
        $page->showme('param-liste',$data);
      }


    function ajout(){
        $data['view']['title'] = "Ajout paramètre";
        $data['action'] = 'add';
        $data['page'] = self::$page;
        $data['r']['patype'] = "";
        $data['r']['pacode'] = "";
        $data['r']['padesi'] = "";
        $data['r']['ordre'] = 0;
        $data['r']['visible'] = 1;
        $page = new Page();
        $page->showme(self::$page.'-edit',$data);
    }
    
    function add(){
           $d = $this->addupd('a');
           $db = db_connect();
           $db->table(self::$table)->insert($d);
           $this->liste();
       }

    function edit($id){
        $data['page'] = self::$page;
        $data['view']['title'] = "Modification ";
        $data['action'] = 'upd';
        $strsql = "select * from ".self::$table." where id = $id";
       // echo $strsql;
        $db = db_connect();
        $query = $db->query($strsql);
        $data['r'] = $query->getRowArray();
        $page = new Page();
        $page->showme(self::$page.'-edit',$data);
    }
   
    public function upd() {
        $id = $this->request->getVar('id');
        $d = $this->addupd('u');
        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->where('id', $id);
        $builder->update($d);
        $this->liste();
    }
       
    function addupd($t){
        $wudate = date('Y-m-d');
        $session = \Config\Services::session();
        $d['patype'] = $this->request->getVar('patype');
        $d['pacode'] = $this->request->getVar('pacode');
        $d['padesi'] = $this->request->getVar('padesi');
        $d['ordre'] = $this->request->getVar('ordre');
        $d['visible'] = $this->request->getVar('visible');
          
        $d['datupd'] = $wudate;
        $d['usrupd'] = $session->userapp;
        if ($t=="a"){
            $d['datcrt'] = $wudate;
            $d['usrcrt'] =  $session->userapp;
            }
        return $d;
        }
 
/* ===== création de la table ===== */

public function crttab(){


$strsql = "
CREATE TABLE IF NOT EXISTS `param` (
    `id` INTEGER,
    `patype`  text,
    `pacode`  text,
    `padesi`  text,
    `patext`  text,
    `ordre`   int,
    `visible` text,
    `usrcrt`  text,
    `datcrt`  text,
    `usrupd`  text,
    `datupd`  text,
    PRIMARY KEY(`id`))";

    echo '<br/>'.$strsql;
    $db = db_connect();
    $db->query($strsql);


}


}
