<?php namespace App\Controllers;

class Uzanto extends BaseController
{
    private static  $table = 'uzanto';
    public function __construct()
    {
   
     $session = \Config\Services::session();
     $userapp = $session->userapp;
     if($userapp == false || $userapp  = NULL || empty($userapp ))
         {
           //  echo view('login.php'); 
           //  exit;
         }
    }
    
    public function index()
	{
        $this->liste();
	}



    public function liste()
	{
        $data['view']['title'] = "Utilisateurs";
        $data['typeapp'] = "tasko";
        $data['dbtable'] = "uzanto";
        $strsql = "select * from uzanto";

        $db = db_connect();
        $query = $db->query($strsql);
        $data['t'] = $query->getResult();
        $page = new Page();
        $page->showme('uzanto-liste',$data);
      }

    /**fait jusque la */
    /************************************************************************************** */
    
    function ajout(){
        $data['dbtable'] = self::$table;
        $data['view']['title'] = "Ajout";
        $data['action'] = 'add';
        $data['r']['ucode'] = "";
        $data['r']['uadmin'] = "";
        $page = new Page();
        $page->showme('uzanto-edit',$data);
    }

    function add(){
        $wudate = date('Y-m-d');
         $session = \Config\Services::session();
         $d = [
            'ucode'  => $this->request->getVar('ucode'),
            'uadmin' => $this->request->getVar('uadmin'),
            'upasvor' => md5($this->request->getVar('upasvor')),
            'udatcrt' => $wudate ,
            'uusrcrt' => $session->userapp,
            'udatupd' => $wudate ,
            'uusrupd' => $session->userapp

        ];

        $db = db_connect();
        $db->table(self::$table)->insert($d);
        $this->liste();
	}

    function edit($id){
        $data['dbtable'] = self::$table;
        $data['view']['title'] = "Modification";
        $data['action'] = 'upd';
        $strsql = "select * from uzanto where uid = $id";  
        $db = db_connect();
        $query = $db->query($strsql);
        $data['r'] = $query->getRowArray();
        $page = new Page();
        $page->showme('uzanto-edit',$data);
    }

	public function upd() {
        helper(['form', 'url']);
        $erreurs = array();
        $pvl = strlen($this->request->getVar('upasvor'));
        if  (( $pvl < 6)  and ($pvl  > 1))
        {
            $erreurs[] = "Un mot de passe doit avoir au moins 6 caractères";
        }

        if (!empty($erreurs)) 
        {
            // on réaffiche le formulaire car il y a une erreur
            $data['dbtable'] = self::$table;
            $data['erreurs'] = $erreurs;
            $data['view']['title'] = "Modification";
            $data['action'] = 'upd';
            $id = $this->request->getVar('uid');
            $strsql = "select * from uzanto where uid = $id";  
            //echo $strsql;
            $db = db_connect();
            $query = $db->query($strsql);
            $data['r'] = $query->getRowArray();
            $page = new Page();
            $page->showme('uzanto-edit',$data);
        }
        else
        {
         //   echo "bon"; 
            
        $id = $this->request->getVar('uid');
        $wudate = date('Y-m-d');
        $session = \Config\Services::session();
        $d['uadmin'] = $this->request->getVar('uadmin');
        $d['udatupd'] = $wudate;
        $d['uusrupd'] = $session->userapp;

        if ($this->request->getVar('upasvor') > ''){
            $d['upasvor'] = $this->request->getVar('upasvor');
            }
      // var_dump($d);

        $db = db_connect();
        $builder = $db->table(self::$table);
        $builder->where('uid', $id);
        $builder->update($d);
        $this->liste(); 
        }
    }

/* ===== suppression etape 1 ===== */
function sup($id){
    $data['dbtable'] = self::$table;
    $data['view']['title'] = "Suppression";
    $data['action'] = 'del';
    $strsql = "select * from uzanto where uid = $id";
    $db = db_connect();
    $query = $db->query($strsql);
    $data['r'] = $query->getRowArray();
    $page = new Page();
    $page->showme('uzanto-edit',$data);
   
}

/* ===== suppression reel ===== */
 function del(){
    $id = $this->request->getVar('uid');
    $db = db_connect();
    $builder = $db->table(self::$table);
    $builder->where('uid', $id);
    $builder->delete();
    $this->liste(); 
}


	//------ A faire 

    public function crttabold(){

    $strsql = "
CREATE TABLE `uzanto2` (
	`uid`	INTEGER,
	`ugrpid`	INTEGER,
	`ucode`	    TEXT,
    `unom`	    TEXT,
    `uemail`    TEXT,
    `uadmin`    TEXT,
    `uremarque  TEXT,
	`upass`	    TEXT,
	`udatcrt`	TEXT,
	`uusrcrt`	TEXT,
	`udatupd`	TEXT,
	`uusrupd`	TEXT,
	PRIMARY KEY(`uid`)
);";
//CREATE TABLE `uzanto` (	`uid` INTEGER,`ugrpid`INTEGER,	`ucode` TEXT, `unom` TEXT, `uemail` TEXT, `uadmin` TEXT, `uremarque  TEXT, `upass` TEXT, `udatcrt`TEXT,`uusrcrt`	TEXT, `udatupd`	TEXT, `uusrupd`	TEXT, PRIMARY KEY(`uid`));
echo $strsql;
}
public function crttab(){
    $db = db_connect();
    $strsql = "DROP TABLE `uzanto`;";
    echo '<br/>'.$strsql;
    $db = db_connect();
   $db->query($strsql);

   $strsql = "CREATE TABLE `uzanto` (
       `uid` INTEGER, 
       `ugrpid` INTEGER, 
       `ucode` TEXT, 
       `unomo` TEXT, 
       `uemail` TEXT, 
       `uadmin` TEXT, 
       `urimarko` TEXT, 
       `upasvor` TEXT, 
       `udatcrt` TEXT, 
       `uusrcrt` TEXT, 
       `udatupd` TEXT,
       `uusrupd` TEXT, 
   PRIMARY KEY(`uid`) )";
    $db->query($strsql);
    $wudate = date('Y-m-d');
    $d = [
        'ucode'  => 'admin',
        'uadmin' => 'A',
        'upasvor' => md5('tasko'),
        'udatcrt' => $wudate ,
        'uusrcrt' => 'admin',
        'udatupd' => $wudate ,
        'uusrupd' => 'admin'
    ];

    $db = db_connect();
    $db->table(self::$table)->insert($d);

}


}

