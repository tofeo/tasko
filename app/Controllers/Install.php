<?php namespace App\Controllers;

class Install extends BaseController
{  


    public function index()
	{
        $erreurs = array();
        $u = $this->request->getVar('u');
        $p = $this->request->getVar('p');
      //  echo strlen($u);
        if (strlen($u) < 2 )
        {
            $erreurs[] = "Un nom d'utilisateur est obligatoire, et il doit avoir au moins deux caractères";
        }

        if (strlen($p) < 6 )
        {
            $erreurs[] = "Un mot de passe doit avoir au moins 6 caractères";
        }
        
        //  $validation =  \Config\Services::validation();
        //  $validation->setRule('u', 'u', 'required');
        if (!empty($erreurs)) {
            //$errors = $validation->getErrors();
            $data['erreurs'] = $erreurs;
            $data['view']['title'] = "Création compte";
            $data['action'] = 'install';
            $data['r']['u'] = $this->request->getVar('u');
            $page = new Page();
            $page->showme('install',$data);
        }
        else
        {
      $wudate = date('Y-m-d');
      $d = [
         'ucode'  => $this->request->getVar('u'),
         'uadmin' => 'A',
         'upasvor' => md5($this->request->getVar('p')),
         'udatcrt' => $wudate ,
         'uusrcrt' => 'installation',
         'udatupd' => $wudate ,
         'uusrupd' => 'installation'
     ];
    // echo "a1";

     $db = db_connect();
     $db->table('uzanto')->insert($d);

     $session = \Config\Services::session($config);
     $session->userapp = $this->request->getVar('u');
     $session->useradmin = 'A';
    
     $data['view']['title'] = "Création compte";
     $data['userapp'] = $u;
     $data['action'] = 'installok';
     $page = new Page();
     $page->showme('install',$data);
//     exit;
        }

    }


}

