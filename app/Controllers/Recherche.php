<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* Recherche */

class Recherche extends CI_Controller {

    private static  $table = 'gestionperso_recherche';
       
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('recherche_model');	
				 
		// on controle ici si une session utilisateur existe
		$userapp = $this->session->userapp;
		if($userapp == false || $userapp  = NULL || empty($userapp )){
			//redirect('login','refresh');
		}
	}

// liste 
	public function index(){
       $this->listesuivi();
	}

	public function list(){
		$data['view']['title'] = "Recherches";
		$data['typeapp'] = "recherche";
		$data['page'] = "recherche/recherche_liste";
		$page = $data['page'];
		$strsql = "SELECT * FROM gestionperso_recherche r LEFT JOIN gestionperso_rechent e on r.rechreid = e.reid order by r.rechdate desc,r.rechid desc ";
		$query = $this->db->query($strsql);
		$data['t'] = $query->result() ;
		//$this->load->view('templates/headerrecherche');
		$this->load->view('page', $data);
	}

	public function listesuivi(){

//echo 'Recherche';
	

		$data['view']['title'] = "Recherches en cours";
		$data['typeapp'] = "recherche";
		$data['page'] = "recherche/recherche_liste";
		$page = $data['page'];
		$query = $this->db->query("SELECT * FROM ".self::$table."  r LEFT JOIN gestionperso_rechent e on r.rechreid = e.reid where rechnegatif <> '1' order by rechdate desc, rechid desc limit 100 ");
		$data['t'] = $query->result() ;
		//$this->load->view('templates/headerrecherche');
		$this->load->view('page', $data);
	}

/* ===== Edition : affichage du formulaire ===== */
	function edit(){
		$data['view']['title'] = "Modification";
		$data['typeapp'] = "recherche";
		$data['action'] = 'upd';
		$id = $this->uri->segment(3);
		$data['r'] = $this->recherche_model->get_1($id);
		
		$data['page'] = "recherche/recherche_edit";
	//	$this->load->view('templates/headerrecherche');
		$this->load->view('page', $data);
	}

/* mise à jour de la table depuis le formulaire*/
	public function upd() {
		$wudate = date('Y-m-d');
$rechnegatif ='0';
if ($this->input->post('rechnegatif') == 'negatif') 				$rechnegatif='1';
	$rechreid = 0;
	$renom = $this->input->post('rechentreprise');
	$rechreid = $this->recherche_model->link_rechreid($renom);
	//echo $rechreid;
		$data = array(
		'rechreid' => $this->input->post('rechreid'),
		'rechdate' => $this->input->post('rechdate'),
		'rechreid' => $rechreid,
		'rechentreprise' => $this->input->post('rechentreprise'),
		'rechdescposte' => $this->input->post('rechdescposte'),
		'rechurl' => $this->input->post('rechurl'),
        'rechnegatif' => $rechnegatif,
        'rechremarque' => $this->input->post('rechremarque'),
		'rechdatupd'=> $wudate ,
		'rechusrupd'=> $this->session->userdata( 'userapp' ));
		$this->db->where('rechid',$this->input->get_post('rechid'));
		$this->db->update(self::$table,$data);
		$this->index();
	}

/* ===== affichage formulaire creation ===== */
	 function ajout(){
        $wudate = date('Y-m-d');
		$data['view']['title'] = "Ajout recherche";
	    $data['typeapp'] = "recherche";
		$data['r']['rechid'] = 0;
		$data['r']['rechdate'] = $wudate;
		$data['r']['rechentreprise'] = '';
		$data['r']['rechdescposte'] = '';
        $data['r']['rechurl'] = '';
        $data['r']['rechnegatif'] = '0';
        $data['r']['rechremarque'] = '';
		$data['action'] = 'add';
		$data['page'] = "recherche/recherche_edit";
		$this->load->view('page', $data);
	}
	 function add(){
		$wudate = date('Y-m-d');

		$data = array(
		'rechdate' => $this->input->post('rechdate'),
		'rechentreprise' => $this->input->post('rechentreprise'),
		'rechdescposte' => $this->input->post('rechdescposte'),
		'rechurl' => $this->input->post('rechurl'),
        'rechremarque' => $this->input->post('rechremarque'),
		'rechusrcrt' => $this->session->userdata( 'userapp' ),
		'rechdatcrt'=> $wudate,
		'rechdatupd'=> $wudate,
		'rechusrupd'=>  $this->session->userdata( 'userapp' ),
		);
		$this->db->insert(self::$table, $data);
		$this->index();
	}

/* ===== suppression etape 1 ===== */
	function sup($id){
		$data['view']['title'] = "Suppression";
		$data['action'] = 'del';
		$id = $this->uri->segment(3);
		$data['r'] = $this->appli_model->get_1($id);
		$data['page'] = "appli/appli_edit";
		$this->load->view('page', $data);
		
	}

/* ===== suppression reel ===== */
	 function del(){
		$id = $this->input->get_post('apid');
							
		$this->db->where('apid',$this->input->get_post('apid'));
		$this->db->delete(self::$table);
		$this->index();
	}

	
	public function creation(){
	$strsql = "CREATE TABLE IF NOT EXISTS `gestionperso_recherche` ( `rechid` TINYINT NOT NULL AUTO_INCREMENT, `rechdate` DATE NOT NULL , `rechentreprise` TEXT NOT NULL , `rechdescposte` TEXT NOT NULL ,`rechmoyen` varchar(1) NOT NULL ,`rechsuspens` varchar(1) NOT NULL , `rechentretien` varchar(1) NOT NULL , `rechnegatif` varchar(1) NOT NULL , `rechremarque` TEXT NOT NULL , `rechdatcrt` DATE NOT NULL , `rechusrcrt` VARCHAR(30) NOT NULL , `rechdatupd` DATE NOT NULL  , `rechusrupd` VARCHAR(30) NOT NULL , UNIQUE (`rechid`)) ENGINE = InnoDB; ";
	echo '<br/>'.$strsql;
	$query = $this->db->query($strsql);
	//$query->result() ;
	}

	public function upd1(){
		$this->load->dbforge();
		$strsql = "rechreid TINYINT NOT NULL AFTER rechid; ";

		if ($this->dbforge->add_column("gestionperso_recherche",$strsql))
		{
			echo "ok <br/>";
			echo $strsql;
		} 
		else
		{
			echo 'non ok';
		}

	}


public function upd2(){
		$this->load->dbforge();
		$strsql = "rechurl text NOT NULL AFTER rechremarque; ";

		if ($this->dbforge->add_column("gestionperso_recherche",$strsql))
		{
			echo "Champ rechurl ajouté <br/>";
			echo $strsql;
		} 
		else
		{
			echo 'non ok';
		}

	}

}

