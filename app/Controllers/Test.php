<?php namespace App\Controllers;
use CodeIgniter\Controller;

class Test extends Controller {

    public function index()
    {
       $this->lang->load('french_lang','french');  
       // $msg = $this->lang->line('test');
        echo 'traduction '.$msg;
       // return view('test');
    }

    public function showme($page = 'home')
    {

        if ( ! is_file(APPPATH.'/Views/tasko/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter

       echo view('template/header', $data);
       echo view('tasko/'.$page.'.php', $data);
       echo view('template/footer', $data);
    }
    public function install()
    {
        echo 'install';
         $contr = new Uzanto();
         $contr->$crttab();
    }
}
