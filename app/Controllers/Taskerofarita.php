<?php namespace App\Controllers;
use App\Models\TaskoModel;
use App\Models\TaskerofaritaModel;


class Taskerofarita extends BaseController
{
    public static  $dbtable = 'taskerofarita';
    public static  $page = 'taskerofarita';
    public function __construct()
    {
       
        $session = \Config\Services::session();
        $userapp = $session->userapp;
        if($userapp == false || $userapp  = NULL || empty($userapp ))
        {
            echo view('login.php'); 
            exit;
        }
    }
    


    public function index()
	{
        $this->liste();
	}



/*    public function liste()
	{
        //  echo 'saisie des réalisations!'. $this->$dbtable ;

        $data['view']['title'] = "Taches";
        $data['typeapp'] = "tasko";
        $data['dbtable'] = self::$dbtable;
        $data['aadatseldeb']  =  date("Y-m-d", strtotime(" - 2 month "));
        $data['aadatselfin']  =  date("Y-m-d");
        
        $session = \Config\Services::session();
        $p['tid'] = 0;
        $p['userapp'] = $session->userapp;
        $p['useradmin'] = $session->useradmin;
        $model = new TaskerofaritaModel();
        $data['tf'] = $model->getTaskerofaritaj($p);
     //  $model->getTaskerofaritaj($p);
        $data['userapp'] = $session->userapp;
        $data['useradmin'] = $session->useradmin;
        $data['view']['title'] = "Réalisations";
        $page = new Page();
        $page->showme('taskerofarita-liste',$data);

    }*/
    
    public function liste()
    {
       /* echo "<br/>date de ".$this->request->getVar('aadatseldeb');
        echo "au ".$this->request->getVar('aadatselfin');
        echo "<br/>date ff ".$this->request->getVar('aadatselfin');*/


        $data['view']['title'] = "Taches";
        $data['typeapp'] = "tasko";
        $data['dbtable'] = self::$dbtable;
        if ($this->request->getVar('aadatseldeb') == ""){
            $aadatseldeb  =  date("Y-m-d", strtotime(" - 2 month "));
        }
        else
        {
            $aadatseldeb  =  $this->request->getVar('aadatseldeb');
        }
        if ($this->request->getVar('aadatselfin') == ""){
            $aadatselfin  =  date("Y-m-d");
        }
        else
        {
            $aadatselfin  =  $this->request->getVar('aadatselfin');
        }
        $data['aadatseldeb']  = $aadatseldeb ;
        $data['aadatselfin']  = $aadatselfin ;
        
        $session = \Config\Services::session();
        $p['tid'] = 0;
        $p['userapp'] = $session->userapp;
        $p['useradmin'] = $session->useradmin;

        $p['aadatseldeb']  = $aadatseldeb ;
        $p['aadatselfin']  =  $aadatselfin ;
        
        $model = new TaskerofaritaModel();
        $data['tf'] = $model->getTaskerofaritaj($p);
     //  $model->getTaskerofaritaj($p);
        $data['userapp'] = $session->userapp;
        $data['useradmin'] = $session->useradmin;
        $data['view']['title'] = "Réalisations";
        $page = new Page();
        $page->showme('taskerofarita-liste',$data);
       // echo $this->request->getVar('tftid');
    }

    function ajout(){

        
        $data['dbtable'] = self::$page;
        $data['view']['title'] = "Ajout d une réalisation";
        $data['action'] = 'add';

        $session = \Config\Services::session();
        $p['userapp'] = $session->userapp;
        $p['useradmin'] = $session->useradmin;
        $data['r']['tftid'] = 0;
        $data['r']['tfdesc'] = "";
        $data['r']['tfduration'] = 0;
        $model = new TaskoModel();
        $data['ldtasko'] = $model->getTaskoDropDown($p);
   
        
        $page = new Page();
        $page->showme('taskerofarita-edit',$data);
    }

    function add(){
        $session = \Config\Services::session();
        
      //  echo 'tftid'. $this->request->getPostGet('tftid');

         $wudate = date('Y-m-d');
         $d = [
            'tftid'  => $this->request->getPostGet('tftid'),
            'tfuzanto' => $session->userapp,
            'tfdate'  => $this->request->getVar('tfdate'),    
            'tfdesc'  => $this->request->getVar('tfdesc'),    
            'tfduration'  => $this->request->getVar('tfduration'),      
            'tfdatcrt' =>  $wudate ,
            'tfusrcrt' => $session->userapp,
            'tfdatupd' =>  $wudate ,
            'tfusrupd' => $session->userapp
        ];
        $db = db_connect();
        $db->table(self::$dbtable)->insert($d);

        // mise à jour de la durée total réel de la tâche
        $id = $this->request->getVar('tftid');
        $model = new TaskoModel();
        $model->updDurationreal($id);
        $model->updDateReal($id,$this->request->getVar('tfdate'));  
        $this->liste();
	}

    function edit($id){
        $session = \Config\Services::session();
        $d['userapp']=$session->userapp;
        $model = new TaskoModel();
        $data['ldtasko'] = $model->getTaskoDropDown($d);
        $data['dbtable'] = self::$dbtable;
        $data['view']['title'] = "Modification d'une réalisation";
        $data['action'] = 'upd';
        $strsql = "select * from ".self::$dbtable." where tfid = $id";
        $db = db_connect();
        $query = $db->query($strsql);
        $data['r'] = $query->getRowArray();
        $page = new Page();
        $page->showme('taskerofarita-edit',$data);   
    }

	public function upd() {
        $date = date('Y-m-d');
        $id = $this->request->getVar('tfid');
        $session = \Config\Services::session();

        $d = [
            'tftid'  => $this->request->getVar('tftid'),
            'tfdate'  => $this->request->getVar('tfdate'),
            'tfdesc'  => $this->request->getVar('tfdesc'),
            'tfduration'  => $this->request->getVar('tfduration'),
            'tfdatupd' =>  $date ,
            'tfusrupd' => $session->userapp
        ];
        $db = db_connect();
        $builder = $db->table(self::$dbtable);
        $builder->where('tfid', $id);
        $builder->update($d);
        // mise à jour de la durée total réel de la tâche
        $id = $this->request->getVar('tftid');
        $model = new TaskoModel();
        $model->updDurationreal($id);
        $this->liste(); 
    }

/* ===== suppression etape 1 ===== */
function sup($id){


    $session = \Config\Services::session($config);
    $d['userapp']=$session->userapp;
    $model = new TaskoModel();
    $data['ldtasko'] = $model->getTaskoDropDown($d);
    $data['dbtable'] = $this->$dbtable;
    $data['view']['title'] = "Modification d'une réalisation";
    $data['action'] = 'del';
    $strsql = "select * from ".$this->$dbtable." where tfid = $id";
    $db = db_connect();
    $query = $db->query($strsql);
    $data['r'] = $query->getRowArray();
    $page = new Page();
    $page->showme('taskerofarita-edit',$data);   
   
}

/* ===== suppression reel ===== */
 function del(){
    $id = $this->request->getVar('tfid');
    $db = db_connect();
    $builder = $db->table($this->$dbtable);
    $builder->where('tfid', $id);
    $builder->delete();

    // mise à jour de la durée total réel de la tâche
    $id = $this->request->getVar('tftid');
    $model = new TaskoModel();
    $model->updDurationreal($id);
    $this->liste(); 
}


	//------ A faire 

	public function crttab(){
        $db = db_connect();
        $strsql = "DROP TABLE `".$this->$dbtable."`;";
        echo '<br/>'.$strsql;
        $db = db_connect();
       $db->query($strsql);
	$strsql = "CREATE TABLE `".$this->$dbtable."` (
        `tfid` INTEGER,
        `tftid` INTEGER,
        `tfuzanto` TEXT,
        `tfdate` TEXT,
        `tfdesc` TEXT,
        `tfduration` INTEGER,
        `tfdatcrt`	TEXT,
    	`tfusrcrt`	TEXT,
    	`tfdatupd`	TEXT,
    	`tfusrupd`	TEXT,
         PRIMARY KEY(`tfid`));";
    echo '<br/>'.$strsql;

    $db->query($strsql);
    }

}

