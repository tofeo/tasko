<?php namespace App\Models;
use CodeIgniter\Model;

class TaskoModel extends Model
{
    protected $table = 'tasko';

    public function getTaskoj($p)
    {
      //echo 'userapp'.$p['userapp'];
      $session = \Config\Services::session();

      $db = db_connect();
      $where = "where tid > 0 ";
      if ($session->useradmin <> 'A')
      {
        $where .= " and tuzanto = '".$p['userapp']."'";
      }

      $listestatus = array("3", "5", "7");
      if (in_array($p['tstatus'], $listestatus)) {
         $where.= " and tstatus = '".$p['tstatus']."'";
      }
      $orderby = " order by tdatreal desc, tid desc";
      $strsql = "select * from tasko ".$where.$orderby;
     //echo $strsql;
      $query = $db->query($strsql);
      $d = $query->getResult();
      return  $d;
    }

    public function get_1($id)
    {
      $db = db_connect();
      $strsql = "SELECT * FROM tasko where tid = ".$id;
    //  echo $strsql;
      $query = $this->db->query($strsql);
      $r = $query->getRow();
    //  var_dump($r);
      return  $r;
    }

    function getTaskoDropDown($p){
      $session = \Config\Services::session();
       $db = db_connect();
       $strsql = "select * from tasko";
       $where = " where tstatus = '5' ";
      // if ($p['useradmin'] <> 'A')
       if ($session->useradmin <> 'A')
       {
         $where .= " and tuzanto = '".$p['userapp']."'";
       }
       $orderby = " order by tdatreal desc, tid desc";
       $strsql = "select * from tasko ".$where.$orderby; 
    //  echo $strsql;
       $query = $db->query($strsql);
       $data['t'] = $query->getResult();
       return  $data['t'];
    }

    public function updDurationreal($id)
    {
      // recherche du nombre du total réalisé
      $strsql = "select sum(tfduration) as duration from taskerofarita where tftid =".$id;
      //echo $strsql;
      $db = db_connect();
      $query = $db->query($strsql);
      $r = $query->getRow();
      $tdurationreal = $r->duration ;
      $builder = $db->table('tasko');
      $builder->where('tid', $id);
      $d = [
        'tdurationreal'  => $r->duration
      ];
      $builder->update($d);
    }
    public function updDateReal($id,$datereal)
    {
      // mise à jour de la date de réalisaiton
      $wudate = date('Y-m-d');
      $db = db_connect();
     // echo 'mise a jour '.$id;
     // echo 'date '.$datereal;
      $builder = $db->table('tasko');
      $builder->where('tid', $id);
      $d = [
        'tdatreal'  =>  $wudate
      ];
      $builder->update($d);
    }
}

