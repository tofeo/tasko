<h1 class="titrepage"><?php echo $view['title'] ;?></h1>

<p> <a href="<?php echo site_url('/'.$dbtable.'/ajout');?>" class="btn btn-primary">Ajout</a></p>
<p> <a href="<?php echo site_url('/'.$dbtable.'/liste/3');?>">A faire</a> -
<a href="<?php echo site_url('/'.$dbtable.'/liste/5');?>">En cours</a> -
<a href="<?php echo site_url('/'.$dbtable.'/liste/7');?>">Terminé</a></p>

<table class="table table-responsive table-striped table-bordered">
      <tr>
            <th></th>
            <th>Tâches</th>
            <th>durée réelle / estimée</th>
            <th>Status</th>
            <th>Réalisation</th>
            <th>Echéance</th>
            <th></th>
      </tr>
      <?php foreach ($t as $r): ?>
      <?php 
            $urledit = site_url($dbtable.'/edit/'.$r->tid);
            $urlvue = site_url($dbtable.'/vue/'.$r->tid);
            $urlsup = site_url($dbtable.'/sup/'.$r->tid);
            ?>
      <tr>
            <td>
                  <a href="<?php echo $urledit;?>">
                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                  </a>  
            </td>
            <td>
            <?php
            if ($r->tstatus =="5"){?>      
   
                  <a href="<?php echo $urlvue;?>">
                  <?php echo $r->tnom;?>
                  </a>
            <?php }
            else
                  {
                   ?>  
 
               <?php echo $r->tnom;?>
            <?php } ?>  

                  <?php if ($useradmin == "A"){echo '('.$r->tuzanto.')';}?>
            </td>

            <td>
                  <?php echo $r->tdurationreal;
                  if ($r->tdurationreal > 0 ) echo '/';
                  echo $r->tdurationestim;?>
            </td>
            <td>
                  <?php
                  switch ($r->tstatus) {
                        case '3':
                            echo "A venir";
                            break;
                        case '5':
                            echo "En cours";
                            break;
                        case '7':
                            echo "Terminé";
                            break;
                    }
                  ?>
            </td>

            <td>
                  <?php
                 echo $r->tdatreal; 
                  ?>
            </td>
          <?php  
          $style ="";
          if ($r->ttermindate < date("Y-m-d")) {$style='style="color:red;"';} 
            echo "<td ".$style.">";
            echo $r->ttermindate; 
            ?>
            </td>
            
<td>
<?php
if ($r->tdurationreal==0) {
echo "<a href=".$urlsup."><i class='fa fa-times text-danger' aria-hidden='true'></i></a>";
//echo "<a href=".$urlsup.">Sup</a>";
}?>

</td>


</tr>
<?php endforeach ?>
</table>
