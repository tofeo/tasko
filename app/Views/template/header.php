<!doctype html>
<html>
<head>
<title>Tasko : Suivi des tâches</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo site_url('../css/bootstrap.min.css');?>">
  <link rel="stylesheet" href="<?php echo site_url('../fa/css/fork-awesome.min.css');?>">
  <link rel="stylesheet" href="<?php echo site_url('../css/tasko.css');?>">

</head>
<body>
<div class="container">
        <?php 
                $session = \Config\Services::session();
                $userapp = $session->userapp;
                $useradmin = $session->useradmin;
                $userid = $session->userid;
        ?>
   
     
<?php
if ($userapp > ''){



?>
<ul class="nav">
  <li class="nav-item">
    <a class="nav-link active" href="<?php echo site_url('taskerofarita');?>">Réalisations</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="<?php echo site_url('tasko');?>">Tâches</a>
  </li>

  <?php
  if ( $useradmin=='A')
  {
    ?>
      <li class="nav-item">
    <a class="nav-link" href="<?php echo site_url('uzanto');?>">Utilisateurs</a>
    </li>
    <li class="nav-item">
    <a class="nav-link" href="<?php echo site_url('param');?>">Paramètres</a>
    </li>
    <?php
  }
  ?>

  <li class="nav-item">
    <a class="nav-link" href="<?php echo site_url('login/logout');?>">Déconnexion</a>
  </li>
  </ul>
<?php
}
?>
