<?php 
                $session = \Config\Services::session();
                $userapp = $session->userapp;
                $useradmin = $session->useradmin;
                $userid = $session->userid;
               // echo 'userapp session :'. $userapp;
        ?>

<h1 class="titrepage">Gestion et suivi des tâches</h1>

 
<div class="container row">
  <div class="col-sm-4" >
  </div>
  <div class="col-sm-4 grey-border arrondi" >
  <?php
  if (!empty($erreurs)) {
    echo "<br/>";
    ?>
  <div class="alert alert-danger" role="alert">
 <p>Attention il y a au moins une erreur</p>
    <?php foreach ($erreurs as $erreur): ?>
        <li><?php echo $erreur; ?></li>
        <?php endforeach ?>
  </div>
  <?php } ?>

  <h2><?php echo $view['title']?></h2>
 

  <?php
  //echo 'action :'.$action;
  if ($action == 'install'){

  helper('form');
  echo form_open("install/index");
  echo "<p><label for='Login'>Identifiant</label><br/>";
  $data = array(
      'name'        => 'u',
      'value'       => 'tasko',
      'style'     => 'width: 100%'
    );
  echo form_input($data);

  ?>
 

  <?php
  echo "<p><label for='p'>Mot de passe</label><br/>";
  $data = array(
    'name'      => 'p',
    'style'     => 'width: 100%'
    

  );
  echo form_password($data);
  echo "</p>";
  $classbouton = "class='btn-block btn-primary'";
  echo form_submit('submit','Connexion',$classbouton);
  echo form_close();  
    }
    if ($action == 'installok'){

   ?>
   <p>Votre application est bien installée.<br/> Un compte administrateur a été créé. N'oubliez pas le mot de passe</p>
   <?php echo "Utilisateur ".$userapp;?>
   <p>
   <a class="nav-link" href="<?php echo site_url('tasko');?>">Continuer</a>
   </p>
<?php
   
    }
  ?>
  <br/>
  </div>
    <div class="col-sm-4" >

    </div>
  </div>
  
  