<h1 class="titrepage"><?php echo $view['title'] ;?></h1>

<p> <a href="<?php echo site_url('/'.$dbtable.'/ajout');?>" class="btn btn-primary">Ajout</a></p>

<table class="table table-responsive table-striped table-bordered">
<?php foreach ($tf as $r): ?>
<tr>
<td>
<?php 
$urledit = site_url($dbtable.'/edit/'.$r->tfid);
$urlsup = site_url($dbtable.'/sup/'.$r->tfid);
?>
<a href="<?php echo $urledit;?>">
<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
</a>
</td>
<?php if ($useradmin == "A"){?>
<td>
<?php echo $r->tfuzanto;?>
</td>
<?php }?>
<td>
<?php echo $r->tfdate;?>
</td>
<td>
<?php echo $r->tnom;?>
<p class="text-secondary"><?php echo $r->tfdesc;?></p>
</td>
<td>
<?php echo $r->tfduration." h";?>
</td>
<td>
<?php

echo "<a href=".$urlsup."><i class='fa fa-times text-danger' aria-hidden='true'></i></a>";
?>
</td>


</tr>
<?php endforeach ?>
</table>