<h1 class="titrepage"><?php echo $view['title'] ;?></h1>
<?php
//
//$page="note";

//echo "page".$page;
helper('form');



if ($action == "add"){ 
    echo form_open($page.'/add'); 
    $txtbouton = "Ajout";
    $classbouton = "class='btn btn-primary'";
}
if ($action == "upd"){ 
    echo form_open($page.'/upd'); 
    $txtbouton = "Mise à jour";
    $classbouton = "class='btn btn-primary'";
}
if ($action == "del"){
     echo form_open($page.'/del'); 
     $txtbouton = "Suppression"; 
     $classbouton = "class='btn btn-danger'";}

?>
<?php
  if (!empty($erreurs)) {
    echo "<br/>";
    ?>
  <div class="alert alert-danger" role="alert">
 <p>Attention il y a au moins une erreur</p>
    <?php foreach ($erreurs as $erreur): ?>
        <li><?php echo $erreur; ?></li>
        <?php endforeach ?>
  </div>
  <?php } ?>

  <div class="form-group row">
    <span class="col-sm-2 col-form-label"> Type</span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'patype',
                        'type'        => 'text',
                        'value'       =>  $r['patype'],
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
    <span class="col-sm-2 col-form-label" >Code</span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'pacode',
                        'type'        => 'text',
                        'value'       =>  $r['pacode'],
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
    <span class="col-sm-2 col-form-label" >Designation</span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'padesi',
                        'type'        => 'text',
                        'value'       =>  $r['padesi'],
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>

    <div class="form-group row">
    <span class="col-sm-2 col-form-label" >Ordre</span>
        <div class="col-10">
        <?php

            $data = array(
                        'name'        => 'ordre',
                        'type'        => 'text',
                        'value'       =>  $r['ordre'],
                        'style'       => 'width: 100%'
                        );
            echo form_input($data);
            ?>
        </div>
    </div>


<div class="form-group row">
    <div class="col-sm-2">
    <?php
      echo form_hidden('visible','1');
    if ($action <> "add"){
    echo form_hidden('id',$r['id']);}

    if ($action <> 'vis'){
      
    echo form_submit('submit',$txtbouton, $classbouton);
    }
    echo form_close();
    ?>
    </div>
</div>
