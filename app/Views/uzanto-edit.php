<h1 class="titrepage"><?php echo $view['title'] ;?></h1>
<?php
helper('form');
if ($action == "add"){ echo form_open($dbtable.'/add'); $txtbouton = "Ajout";}
if ($action == "upd"){ echo form_open($dbtable.'/upd'); $txtbouton = "Mise à jour";}
if ($action == "del"){ echo form_open($dbtable.'/del'); $txtbouton = "Suppression";}

if ($action == "add"){ 
    echo form_open($dbtable.'/add'); 
    $txtbouton = "Ajout";
    $classbouton = "class='btn btn-primary'";
}
if ($action == "upd"){ 
    echo form_open($dbtable.'/upd'); 
    $txtbouton = "Mise à jour";
    $classbouton = "class='btn btn-primary'";
}
if ($action == "del"){
     echo form_open($dbtable.'/del'); 
     $txtbouton = "Suppression"; 
     $classbouton = "class='btn btn-danger'";}

?>
<?php
  if (!empty($erreurs)) {
    echo "<br/>";
    ?>
  <div class="alert alert-danger" role="alert">
 <p>Attention il y a au moins une erreur</p>
    <?php foreach ($erreurs as $erreur): ?>
        <li><?php echo $erreur; ?></li>
        <?php endforeach ?>
  </div>
  <?php } ?>


<div class="form-group row">





    <span class="col-sm-2 col-form-label" >Pseudo</span>
    <div class="col-10">

        <?php
        if ($action == "add")
        {
            $data = array(
                'name'        => 'ucode',
                'type'        => 'text',
                'value'       => $r['ucode'],
                'style'       => 'width: 100%'
                );
            echo form_input($data);
        }
        else
        {
            echo $r['ucode'];
        }
      
        ?>
    </div>
</div>

<div class="form-group row">
    <span class="col-sm-2 col-form-label" >Mot de passe</span>
    <div class="col-10">
        <?php
        $data = array(
                    'name'        => 'upasvor',
                    'type'        => 'text',
                    'value'       => '',
                    'style'       => 'width: 100%'
                    );
        echo form_password($data);
        ?>
    </div>
</div>
<?php if ($useradmin == "A"){
?>
<div class="form-group row">
    <span class="col-sm-2 col-form-label" >Permission</span>
    <div class="col-10">
        <select name="uadmin">
        <?php
        $seladmin="";
        if ($r['uadmin'] =="A"){$seladmin="selected";}
        ?>
        <option value="">Utilisateur</a>
        <option value="A" <?php echo $seladmin; ?>>Administrateur</a>
        </select>
    </div>            

</div>
<?php     
}
?>

<?php
    if ($action != "add"){
    ?>

<div class="form-group row">
    <span class="col-sm-2 text-secondary">Création </span>
    <span class="col-sm-2 text-secondary"><?php echo $r['udatcrt'];?> </span>
    <span class="col-sm-2 text-secondary"> <?php echo $r['uusrcrt'];?> </span>
</div>
<div class="row">
    <span class="col-sm-2 text-secondary">Modification </span>
    <span class="col-sm-2 text-secondary"><?php echo $r['udatupd'];?></span>
    <span class="col-sm-2 text-secondary"> <?php echo $r['uusrupd'];?> </span>
</div>

<?php     
}
?>
<div class="form-group row">
    <div class="col-sm-2">
    <?php
    if ($action <> "add"){
    echo form_hidden('uid',$r['uid']);}

    if ($action <> 'vis'){
    echo form_submit('submit',$txtbouton, $classbouton);
    }
    echo form_close();
    ?>
    </div>
</div>
