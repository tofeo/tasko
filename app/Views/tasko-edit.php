<h1 class="titrepage"><?php echo $view['title'] ;?></h1>


<?php //echo "nombre ".$nbtf ;?>
<?php
helper('form');

if ($action == "add"){ echo form_open('tasko/add'); $txtbouton = "Ajout";}
if ($action == "upd"){ echo form_open('tasko/upd'); $txtbouton = "Mise à jour";}
if ($action == "del"){ echo form_open('tasko/del'); $txtbouton = "Suppression";}

if ($action == "add"){ 
    echo form_open($dbtable.'/add'); 
    $txtbouton = "Ajout";
    $classbouton = "class='btn btn-primary'";
}
if ($action == "upd"){ 
    echo form_open($dbtable.'/upd'); 
    $txtbouton = "Mise à jour";
    $classbouton = "class='btn btn-primary'";
}
if ($action == "del"){
     echo form_open($dbtable.'/del'); 
     $txtbouton = "Suppression"; 
     $classbouton = "class='btn btn-danger'";}
?>

<div class="form-group row">
    <span class="col-sm-2 col-form-label" >Tâche</span>
    <div class="col-10">
        <?php
        $data = array(
                    'name'        => 'tnom',
                    'type'        => 'text',
                    'value'       => $r['tnom'],
                    'style'       => 'width: 100%'
                    );
        echo form_input($data);
        ?>
    </div>
</div>

<div class="form-group row">
    <span class="col-sm-2 col-form-label" >Description</span>
    <div class="col-10">
    <?php
  //  if ($action == "add"){ $tfdate =  date('Y-m-d');} else{$tfdate = $r['tfdate'];}
    $data = array(
                'name'        => 'tdesc',
                'type'        => 'text',
                'value'       => $r['tdesc'],
                'style'       => 'width: 100%'
                );
    echo form_textarea($data);
    ?>
    </div>
</div>

<div class="form-group row">
        <span class="col-sm-2 col-form-label" >Catégorie</span>
        <div class="col-4">
            <select name="tcateg" class="form-control">
                <?php 
                foreach($categs as $row)
                { 
                    $selected ='';
                    if ($r['tcateg'] == $row->pacode) $selected = "selected";
                echo '<option value="'.$row->pacode.'" '.$selected.'>'.$row->padesi.'</option>';
                }
                ?>
            </select>
        </div>
    </div>

<div class="form-group row">
    <span class="col-sm-2 col-form-label" >Durée réelle en heure</span>
    <div class="col-10">
        <?php echo $r['tdurationreal'];?>
    </div>
</div>

<div class="form-group row">
    <span class="col-sm-2 col-form-label" >Durée estimée en heures</span>
    <div class="col-10">
        <?php
        $data = array(
            'name'        => 'tdurationestim',
            'type'        => 'text',
            'id'          => 'tdurationestim',
            'value'       => $r['tdurationestim'],
            'style'       => 'width: 100%'
       );
        echo form_input($data);
        ?>
    </div>
</div>


<div class="form-group row">
    <span class="col-sm-2 col-form-label" >Date échéance</span>
    <div class="col-10">
        <?php
        $data = array(
            'name'        => 'ttermindate',
            'type'        => 'date',
            'id'          => 'ttermindate',
            'value'       => $r['ttermindate'],
            'style'       => 'form-control'
       );
        echo form_input($data);
        ?>
    </div>
</div>

<div class="form-group row">
    <span class="col-sm-2 col-form-label" >Etat</span>
    <div class="col-10">
        <select name="tstatus">
        <?php
        if ($action == "add") 
        {
            $tstat = '5';
        }
        else
        {
            $tstat = $r['tstatus'];
        }
          // 3 à venir  5 en cours et 7 cloturé
        $seladmin="";
        $sel3 = "";
        $sel5 = "";
        $sel7 = "";
        if ($tstat == "3"){$sel3 = "selected";}
        if ($tstat == "5"){$sel5 = "selected";}
        if ($tstat == "7"){$sel7 = "selected";}
        ?>
        <option value="3" <?php echo $sel3; ?>>A venir</a>
        <option value="5" <?php echo $sel5; ?>>En cours</a>
        <option value="7" <?php echo $sel7; ?>>Cloturé</a>
        </select>
    </div>
</div>

<?php
    if ($action != "add"){
    ?>
<div class="form-group row">
    <span class="col-sm-2 text-secondary">Propriétaire</span>
    <span class="col-sm-2 text-secondary"> <?php echo $r['tuzanto'];?></span>
    <span class="col-sm-2 text-secondary"> <?php echo $r['tdatreal'];?> </span>
</div>

<div class="form-group row">
    <span class="col-sm-2 text-secondary">Création </span>
    <span class="col-sm-2 text-secondary"><?php echo $r['tdatcrt'];?> </span>
    <span class="col-sm-2 text-secondary"> <?php echo $r['tusrcrt'];?> </span>
</div>
<div class="row">
    <span class="col-sm-2 text-secondary">Modification </span>
    <span class="col-sm-2 text-secondary"><?php echo $r['tdatupd'];?></span>
    <span class="col-sm-2 text-secondary"> <?php echo $r['tusrupd'];?> </span>
</div>
<?php
echo form_hidden('tuzanto',$r['tuzanto']);
    }
    ?>

<div>
<?php

if ($action <> "add"){
echo form_hidden('tid',$r['tid']);}

if ($action <> 'vis'){
echo form_submit('submit',$txtbouton, $classbouton);
}

echo form_close();
//echo $nbtf;
?>
</div>