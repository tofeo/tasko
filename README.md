# tasko

## But

Gestion des tâches simples. Chaque jour, saisissez ce que vous faites. Vous aurez ainsi un journal des tâches réalisées, des statisques du temps passé par tâchem par période etc.. Cette application est idéal pour un suivi personnel ou pour une petite équipe. 

## Installation

Attention cette appllication est en développement. Une installation peut se faire dans un but de test. Il n'est pas recommandé de faire une installation en production. 

Prérequis
*PHP version 7.2* or newer is required, with the *intl* extension installed.

The following PHP extensions should be enabled on your server: php-json, php-mbstring, php-mysqlnd, php-xml`

In order to use the CURLRequest, you will need libcurl installed.

Tasko est basé sur CodeIgniter 4. Donc vous devez avoir un serveur web avec php 7.2 min Pour les prérequis voir ceux de CI4 

https://codeigniter4.github.io/userguide/intro/requirements.html


Il y a plusieurs façons d'installer

### En copiant les sources par ftp
1. Télécharger les sources
2. Décompresser 
3. Renommer le fichier env qui se trouve sur la racine en .env 
4. Editer le fichier .env et modifier l'url du site
5. Envoyer les sources sur le serveur
6. Avec un navigateur lancer l'application http://votresite/tasko Vous devriez arrivez sur une page login. utilisateur : admin mot de passe tasko
7. Changer le mot de passe admin
8. Il est recommander de créer un utilisateur non admin qui vous servira au quotidien.


### avec git

avec ssh sur votre serveur
cd /repertoir/source/html
sudo git clone https://framagit.org/tofeo/tasko.git


## A venir
### Version 1 
* Géstion des tâches
* Saisie des réalisations
* Visualisation des réalisation sur une durée par jour, semaine ou mois. Comparaison éventuelle de la durée estimée et la durée réelle.
* Version responsive 
* installateur et procédure d'installation

### idées pour l'avenir
* Tratuction en anglais
* Ajout d'une gestion de projet : un projet a des tâches. 

version 2019-10
